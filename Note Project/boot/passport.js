var config = require("nconf");
var passport = require("passport");
var mysql = require("mysql");
var crypto = require("crypto");
var LocalStrategy = require("passport-local").Strategy;

passport.use("local", new LocalStrategy(function(username, password, done) {

	var md5 = crypto.createHash("md5");
	password = md5.update(password).digest("hex");

	var connection = mysql.createConnection({
		host : config.get("mysql:host"),
		user : config.get("mysql:user"),
		password : config.get("mysql:password"),
	});

	connection.connect();
	connection.query("SELECT `name`, `id` FROM `node_js`.`users` WHERE `name` = '" + username + "' AND `password` = '" + password + "' LIMIT 1", function(err, rows, fields) {

		if (rows.length > 0) {

			var result = rows[0];

			done(null, {
				name : result.name,
				id : result.id,
			});

		} else {
			done(null, false, {
				message : "Неверный логин или пароль",
			});
		}
	});
	connection.end();
}));

passport.serializeUser(function(user, done) {
	done(null, JSON.stringify(user));
});

passport.deserializeUser(function(data, done) {
	done(null, JSON.parse(data));
});

module.exports = function(app) {
};