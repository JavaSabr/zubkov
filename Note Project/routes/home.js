var config = require("nconf");
var mysql = require("mysql");

// сортировщик заметок
function compare(first, second) {
	return second.create_time - first.create_time;
}

module.exports = function(app) {
	app.get("/", function(req, res) {

		var currentUser = req.user;

		if (currentUser === undefined) {
			res.render("index.ejs");
			return;
		}

		var connection = mysql.createConnection({
			host : config.get("mysql:host"),
			user : config.get("mysql:user"),
			password : config.get("mysql:password"),
		});

		connection.connect();
		connection.query("SELECT * FROM `node_js`.`notes` WHERE `user_id` = " + currentUser.id, function(err, rows, fields) {

			rows.sort(compare);

			res.render("index.ejs", {
				user : currentUser,
				notes : rows
			});
		});
		connection.end();
	});

	app.post("/add-note", function(req, res) {

		var currentUser = req.user;

		if (currentUser === undefined) {
			res.render("index.ejs");
			return;
		}

		var body = req.body;
		var content = body.content;

		if (typeof content === undefined) {
			return;
		}

		if (content.length < 1) {
			res.render("index.ejs");
			return;
		}

		var connection = mysql.createConnection({
			host : config.get("mysql:host"),
			user : config.get("mysql:user"),
			password : config.get("mysql:password"),
		});

		connection.connect();
		connection.query("INSERT INTO `node_js`.`notes` (`user_id`, `content`, `create_time`) VALUES ('" + currentUser.id + "', '" + content + "', '" + Date.now() + "')", function(err, rows, fields) {
			res.redirect("/");
		});
		connection.end();
	});

	app.post("/remove-note", function(req, res) {

		var currentUser = req.user;

		if (currentUser === undefined) {
			res.render("index.ejs");
			return;
		}

		var body = req.body;
		var id = body.id;

		if (typeof id === undefined) {
			return;
		}

		if (id.length < 1) {
			res.render("index.ejs");
			return;
		}

		var connection = mysql.createConnection({
			host : config.get("mysql:host"),
			user : config.get("mysql:user"),
			password : config.get("mysql:password"),
		});

		connection.connect();
		connection.query("DELETE FROM `node_js`.`notes` WHERE `id` = " + id + " LIMIT 1", function(err, rows, fields) {
			res.redirect("/");
		});
		connection.end();
	});
};