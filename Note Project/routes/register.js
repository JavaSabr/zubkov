var passport = require('passport');
var mysql = require("mysql");
var crypto = require("crypto");
var config = require("nconf");

module.exports = function(app) {

	app.get("/register", function(req, res) {
		res.render("register.ejs", {
			error : req.flash('error')
		});
	});

	app.post("/register", function(req, res) {

		if (req.isAuthenticated()) {
			res.redirect("/");
			return;
		}

		var body = req.body;
		var user = body.user;

		if (typeof user === undefined) {
			return;
		}

		var name = user.name;
		var password = user.password;
		var confirm = user.confirm;

		if (name.length < 1 || password.length < 1 || confirm.length < 1) {

			res.render("register.ejs", {
				error : "Не все данные введены",
			});

			return;
		}

		if (password !== confirm) {

			res.render("register.ejs", {
				error : "Введенные пароли не совпадают",
			});

			return;
		}

		if (name.length < 2) {

			res.render("register.ejs", {
				error : "Введенное имя слишком короткое",
			});

			return;
		}

		if (password.length < 4) {

			res.render("register.ejs", {
				error : "Введенный пароль слишком короткий",
			});

			return;
		}

		var md5 = crypto.createHash("md5");
		var hashPassword = md5.update(password).digest("hex");

		var connection = mysql.createConnection({
			host : config.get("mysql:host"),
			user : config.get("mysql:user"),
			password : config.get("mysql:password"),
		});

		connection.connect();
		connection.query("INSERT INTO `node_js`.`users` (`name`, `password`) VALUES ('" + name + "', '" + hashPassword + "')", function(err, rows, fields) {

			if (err !== null) {
				res.render("register.ejs", {
					error : "Пользователь с таким именем уже существует",
				});
			} else {
				res.redirect("/auth");
			}
		});
		connection.end();
	});
};