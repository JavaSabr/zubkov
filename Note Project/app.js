var config = require("nconf");
var express = require("express");
var http = require("http");
var mysql = require("mysql");
var app = express();

config.argv().env().file({
	file : "config.json"
});

// boot
require("./boot/index")(app);
// routing
require("./routes/index")(app);

var connection = mysql.createConnection({
	host : config.get("mysql:host"),
	user : config.get("mysql:user"),
	password : config.get("mysql:password"),
});

console.log("MySQL: check connections...");

connection.connect();
connection.query("SELECT COUNT(*) FROM node_js.users", function(err, rows, fields) {

	if (err) {
		throw err;
	}

	console.log("MySQL: registered users: ", rows.length);

	http.createServer(app).listen(app.get("port"), function() {
		if ("development" === app.get("env")) {
			console.log("Server: started on port " + app.get("port"));
		}
	});
});

connection.end();